package com.samuel.desafioandroid;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.samuel.desafioandroid.interfaces.DialogInterface;
import com.samuel.desafioandroid.utils.Utils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by samuel on 16/01/18.
 */

public class BaseActivity extends MvpAppCompatActivity {

    protected boolean runningBackground = false;
    protected Toolbar toolbar;
    protected TextView toolbarTitle;
    private SweetAlertDialog dialogProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDialogProgress();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        runningBackground = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        runningBackground = true;
    }

    public void setUpToolbarText(int title, boolean isBack) {
        setUpToolbarText(getString(title), isBack);
    }

    public void setUpToolbarText(String title, boolean isBack) {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            if (!TextUtils.isEmpty(title)) {
                getSupportActionBar().setDisplayShowTitleEnabled(true);
                getSupportActionBar().setTitle(title);
            } else {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
            if (isBack) {
                toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
                toolbar.setNavigationOnClickListener(v -> onBackPressed());
            }
        }
    }

    private void initDialogProgress() {
        dialogProgress = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        dialogProgress.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        dialogProgress.setCancelable(false);
        dialogProgress.setTitleText(getString(R.string.please_wait));
    }

    public void showErrors(Throwable t, DialogInterface callback) {
        Utils.showDialogThrowable(this, t, callback);
    }
}
