package com.samuel.desafioandroid.interfaces;

import java.io.Serializable;

/**
 * Created by samuel on 16/01/18.
 */

public interface DialogInterface extends Serializable {
    void onDismiss();
}
