package com.samuel.desafioandroid.presentation.presenter.activity;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.samuel.desafioandroid.presentation.view.activity.PullRequestView;
import com.samuel.desafioandroid.service.model.pull_request.DataDetailPullRequest;
import com.samuel.desafioandroid.ui.adapter.PullRequestAdapter;
import com.samuel.desafioandroid.utils.Utils;

@InjectViewState
public class PullRequestPresenter extends MvpPresenter<PullRequestView> {

    private DataDetailPullRequest dataDetailPullRequest;
    private PullRequestAdapter adapter;

    public PullRequestPresenter(DataDetailPullRequest dataDetailPullRequest) {
        this.dataDetailPullRequest = dataDetailPullRequest;
        setData();
    }

    private void setData() {
        getViewState().setTitleToolbar(dataDetailPullRequest.getRepositoryModel().getName());
        getViewState().setIssuesOpen(String.valueOf(Utils.getNumFormat(dataDetailPullRequest.getRepositoryModel().getOpenIssues())));
        getViewState().setStart(String.valueOf(Utils.getNumFormat(dataDetailPullRequest.getRepositoryModel().getStargazersCount())));
        getViewState().setFork(String.valueOf(Utils.getNumFormat(dataDetailPullRequest.getRepositoryModel().getForks())));
        initAdapter();
    }

    private void initAdapter() {
        if (dataDetailPullRequest.getListPullRequest().size() > 0) {
            adapter = new PullRequestAdapter(dataDetailPullRequest.getListPullRequest());
            getViewState().setAdapter(adapter);
        } else {
            getViewState().showPlaceholder();
        }
    }
}
