package com.samuel.desafioandroid.presentation.presenter.activity;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.samuel.desafioandroid.presentation.view.activity.HomeView;
import com.samuel.desafioandroid.service.RetrofitBase;
import com.samuel.desafioandroid.service.model.repository.Item;
import com.samuel.desafioandroid.service.model.repository.Owner;
import com.samuel.desafioandroid.service.model.repository.RepoResponse;
import com.samuel.desafioandroid.ui.adapter.RepositoryAdapter;

import java.util.HashMap;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView> {

    public static final String LANGUAGE = "language:Java";
    public static final String SORT = "stars";

    HashMap<String, Owner> peopleCache = new HashMap<>();

    private int page = 1;
    private boolean isMore = false;
    private RepositoryAdapter repositoryAdapter;

    public HomePresenter() {
        getViewState().init();
        getViewState().setListenerList(getScrollListener());
        getRepositories();
    }

    public void getRepositories() {
        getViewState().onLoading(true);
        RetrofitBase.getRetrofitApi()
                .getRepositories(LANGUAGE,
                        SORT,
                        page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSearchReposSuccess,
                        this::onSearchReposError);
    }

    private void onSearchReposSuccess(RepoResponse repoResponse) {
        getViewState().onLoading(false);
        List<Item> listRepositorios = repoResponse.getItems();
        if (listRepositorios.size() == 30) {
            isMore = true;
            page++;
        } else {
            isMore = false;
        }
        initAdapter(listRepositorios);
    }

    private void onSearchReposError(Throwable throwable) {
        getViewState().onLoading(false);
        getViewState().showErrorDialog(throwable);
    }

    private void initAdapter(List<Item> list) {
        if (list.size() > 0) {
            if (repositoryAdapter == null) {
                repositoryAdapter = new RepositoryAdapter(list);
                repositoryAdapter.setHomeView(getViewState());
                getViewState().setAdapter(repositoryAdapter);
            } else {
                repositoryAdapter.addItens(list);
            }
        }
    }

    public void refreshList() {
        page = 1;
        isMore = false;
        if (repositoryAdapter != null) repositoryAdapter.clear();
        getRepositories();
    }

    private RecyclerView.OnScrollListener getScrollListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    int pastVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if (((visibleItemCount + pastVisibleItems) >= totalItemCount)) {
                        if (isMore) {
                            isMore = false;
                            getRepositories();
                        }
                    }
                }
            }
        };
    }
}
