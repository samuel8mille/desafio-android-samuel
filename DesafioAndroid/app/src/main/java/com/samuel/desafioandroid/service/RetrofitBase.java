package com.samuel.desafioandroid.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.samuel.desafioandroid.BuildConfig;
import com.samuel.desafioandroid.app.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by samuel on 16/01/18.
 */

public final class RetrofitBase {

    private static RestApi retrofitApi;
    private Retrofit retrofit;

    private RetrofitBase() {
        initRetrofit();
    }

    public static RestApi getRetrofitApi() {
        if (retrofitApi == null) new RetrofitBase();
        return retrofitApi;
    }

    private void initRetrofit() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        String urlBase = Constants.Service.BASE_URL;

        OkHttpClient httpClient = builder.build();

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(gson);

        retrofit = new Retrofit.Builder()
                .baseUrl(urlBase)
                .client(httpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .build();

        retrofitApi = retrofit.create(RestApi.class);
    }
}
