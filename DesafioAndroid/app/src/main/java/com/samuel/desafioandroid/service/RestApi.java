package com.samuel.desafioandroid.service;

import com.samuel.desafioandroid.app.Constants;
import com.samuel.desafioandroid.service.model.pull_request.PullRequestModel;
import com.samuel.desafioandroid.service.model.repository.Owner;
import com.samuel.desafioandroid.service.model.repository.RepoResponse;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by samuel on 16/01/18.
 */

public interface RestApi {

    @GET(Constants.Service.SEARCH)
    Observable<RepoResponse> getRepositories(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

    @GET(Constants.Service.REPOS)
    Observable<List<PullRequestModel>> getPullRequests(@Path("creator") String creator, @Path("repo") String repo, @Query("client_id") String clientId, @Query("client_secret") String clientSecret);

    @GET(Constants.Service.GET_OWNER)
    Observable<Owner> getOwner(@Path("user") String user, @Query("client_id") String clientId, @Query("client_secret") String clientSecret);
}
