package com.samuel.desafioandroid.ui.activity.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.samuel.desafioandroid.BaseActivity;
import com.samuel.desafioandroid.R;
import com.samuel.desafioandroid.app.Constants;
import com.samuel.desafioandroid.presentation.presenter.activity.PullRequestPresenter;
import com.samuel.desafioandroid.presentation.view.activity.PullRequestView;
import com.samuel.desafioandroid.service.model.pull_request.DataDetailPullRequest;
import com.samuel.desafioandroid.ui.adapter.PullRequestAdapter;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestActivity extends BaseActivity implements PullRequestView {

    public static final String TAG = "PullRequestActivity";
    @InjectPresenter
    PullRequestPresenter mPullRequestPresenter;

    @BindView(R.id.txt_issue_opened)
    TextView txtIssueOpend;
    @BindView(R.id.list_pull_request)
    RecyclerView listPullRequest;

    @BindView(R.id.txt_no_pull_request)
    TextView txtNoPullRequest;

    @BindView(R.id.txt_start)
    TextView txtStart;
    @BindView(R.id.txt_fork)
    TextView txtFork;

    public static void open(Context context, DataDetailPullRequest dataDetailPullRequest) {
        Intent intent = new Intent(context, PullRequestActivity.class);
        intent.putExtra(Constants.EXTRA_DATA_PULL_REQUEST, Parcels.wrap(dataDetailPullRequest));
        context.startActivity(intent);
    }

    @ProvidePresenter
    public PullRequestPresenter provideDetailsPresenter() {
        return new PullRequestPresenter(Parcels.unwrap(getIntent().getParcelableExtra(Constants.EXTRA_DATA_PULL_REQUEST)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        ButterKnife.bind(this);
    }

    @Override
    public void setTitleToolbar(String title) {
        setUpToolbarText(title, true);
    }

    @Override
    public void setIssuesOpen(String issuesOpen) {
        txtIssueOpend.setText(String.format(getString(R.string.issues_opened), issuesOpen));
    }

    @Override
    public void setStart(String star) {
        txtStart.setText(String.format(getString(R.string.star), star));
    }

    @Override
    public void setFork(String qtdeFork) {
        txtFork.setText(String.format(getString(R.string.fork), qtdeFork));
    }

    @Override
    public void setAdapter(PullRequestAdapter adapter) {
        listPullRequest.setLayoutManager(new LinearLayoutManager(this));
        listPullRequest.setAdapter(adapter);
    }

    @Override
    public void showPlaceholder() {
        txtNoPullRequest.setVisibility(View.VISIBLE);
    }
}
