package com.samuel.desafioandroid.presentation.view.adapter;

import com.arellomobile.mvp.MvpView;
import com.samuel.desafioandroid.service.model.pull_request.PullRequestModel;

/**
 * Created by samuel on 18/01/18.
 */

public interface PullRequestAdapterCallback extends MvpView {

    void setTitle(String title);

    void setDescricao(String descricao);

    void setDataPullRequest(String data);

    void setImage(String pathImage);

    void setNomeUsuario(String nome);

    void setCategoria(String categoria);

    void openWeb(String url);

    void updateFullNameList(PullRequestModel item);

    void setFullName(String nameUser);

    void showErrorMessage(Throwable throwable);
}
