package com.samuel.desafioandroid.ui.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.samuel.desafioandroid.R;
import com.samuel.desafioandroid.interfaces.DialogInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samuel on 16/01/18.
 */

public class ErrorDialog extends DialogFragment {

    public static final String BUNDLE_MESSAGE = "bundle_message";
    public static final String BUNDLE_CALLBACK = "bundle_callback";
    public static final int DIALOG_TIME = 5000;

    @BindView(R.id.txt_message)
    TextView txtMessage;

    private DialogInterface callback;
    private String message;

    public static void showErrorMessage(FragmentManager fragmentManager, String messsage, DialogInterface callback) {
        ErrorDialog dialog = new ErrorDialog();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_MESSAGE, messsage);
        bundle.putSerializable(BUNDLE_CALLBACK, callback);
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, "dialog_alerta_criado");
    }

    public static void showErrorMessage(FragmentManager fragmentManager, String messsage) {
        ErrorDialog dialog = new ErrorDialog();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_MESSAGE, messsage);
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, "dialog_alerta_criado");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.ThemeDialog);
        setCancelable(false);
        if (getArguments() != null && getArguments().get(BUNDLE_CALLBACK) != null) {
            callback = (DialogInterface) getArguments().get(BUNDLE_CALLBACK);
        }
        if (getArguments() != null && getArguments().get(BUNDLE_MESSAGE) != null) {
            message = getArguments().getString(BUNDLE_MESSAGE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.error_dialog, null);
        ButterKnife.bind(this, view);
        txtMessage.setText(message);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            ErrorDialog.this.dismiss();
            if (callback != null) {
                callback.onDismiss();
            }
        }, DIALOG_TIME);
        return view;
    }

    @Override
    public void onPause() {
        ErrorDialog.this.dismiss();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
