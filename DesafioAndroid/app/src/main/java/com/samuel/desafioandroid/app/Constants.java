package com.samuel.desafioandroid.app;

/**
 * Created by samuel on 16/01/18.
 */

public class Constants {

    public static final String EXTRA_DATA_PULL_REQUEST = "extra_data_pull_request";
    public static final String CLIENT_ID = "45a982af7a64eb9123f5";
    public static final String CLIENT_SECRET = "e93c4279e0109eaa841bd9e6e5257918dcabe7e9";

    public static class Service {
        public static final String BASE_URL = "https://api.github.com/";
        public static final String SEARCH = "search/repositories";
        public static final String REPOS = "repos/{creator}/{repo}/pulls";
        public static final String GET_OWNER = "users/{user}";
    }
}
