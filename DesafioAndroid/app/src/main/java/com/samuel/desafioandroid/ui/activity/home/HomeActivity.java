package com.samuel.desafioandroid.ui.activity.home;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.samuel.desafioandroid.BaseActivity;
import com.samuel.desafioandroid.R;
import com.samuel.desafioandroid.presentation.presenter.activity.HomePresenter;
import com.samuel.desafioandroid.presentation.view.activity.HomeView;
import com.samuel.desafioandroid.ui.adapter.RepositoryAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements HomeView, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "HomeActivity";

    @BindView(R.id.repo_list)
    RecyclerView repoList;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @InjectPresenter
    HomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setUpToolbarText(R.string.home_title, false);
    }

    @Override
    public void init() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void setListenerList(RecyclerView.OnScrollListener scrollListener) {
        repoList.addOnScrollListener(scrollListener);
    }

    @Override
    public void onLoading(boolean status) {
        swipeRefreshLayout.setRefreshing(status);
    }

    @Override
    public void setAdapter(RepositoryAdapter repositoryAdapter) {
        repoList.setLayoutManager(new LinearLayoutManager(this));
        repoList.setAdapter(repositoryAdapter);
    }

    @Override
    public void onRefresh() {
        presenter.refreshList();
    }

    @Override
    public void showErrorDialog(Throwable throwable) {
        showErrors(throwable, null);
    }
}
