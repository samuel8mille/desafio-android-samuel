package com.samuel.desafioandroid.presentation.presenter.adapter;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.samuel.desafioandroid.app.Constants;
import com.samuel.desafioandroid.presentation.view.adapter.PullRequestAdapterCallback;
import com.samuel.desafioandroid.service.RetrofitBase;
import com.samuel.desafioandroid.service.model.pull_request.PullRequestModel;
import com.samuel.desafioandroid.service.model.repository.Owner;
import com.samuel.desafioandroid.utils.Utils;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by samuel on 18/01/18.
 */
@InjectViewState
public class PullRequestAdapterPresenter extends MvpPresenter<PullRequestAdapterCallback> {

    private PullRequestModel pullRequestModel;

    public PullRequestAdapterPresenter() {
    }

    public void setData(PullRequestModel pullRequestModel) {
        this.pullRequestModel = pullRequestModel;

        getViewState().setTitle(pullRequestModel.getTitle());
        getViewState().setDescricao(pullRequestModel.getBody());
        getViewState().setDataPullRequest(Utils.getDateRequest(pullRequestModel.getCreated_at()));

        getViewState().setNomeUsuario(pullRequestModel.getUser().getLogin());
        getViewState().setImage(pullRequestModel.getUser().getAvatarUrl());
        getViewState().setCategoria(pullRequestModel.getUser().getType());
        getOwner(pullRequestModel.getUser().getLogin());
    }

    public void getOwner(String owner) {
        if (TextUtils.isEmpty(pullRequestModel.getNameUser())) {
            RetrofitBase.getRetrofitApi()
                    .getOwner(owner,
                            Constants.CLIENT_ID,
                            Constants.CLIENT_SECRET)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::getOwnerSuccess,
                            this::getOwnerError);
        } else {
            getViewState().setFullName(pullRequestModel.getNameUser());
        }
    }

    private void getOwnerSuccess(Owner owner) {
        if (owner.getName() != null) {
            pullRequestModel.setNameUser(owner.getName());
            getViewState().setFullName(owner.getName());
            getViewState().updateFullNameList(pullRequestModel);
        } else {
            pullRequestModel.setNameUser("-");
            getViewState().setFullName("-");
            getViewState().updateFullNameList(pullRequestModel);
        }
    }

    private void getOwnerError(Throwable throwable) {
        getViewState().showErrorMessage(throwable);
    }

    public void clickDetalhe() {
        getViewState().openWeb(pullRequestModel.getHtml_url());
    }
}
