package com.samuel.desafioandroid.presentation.view.adapter;

import com.arellomobile.mvp.MvpView;
import com.samuel.desafioandroid.service.model.pull_request.DataDetailPullRequest;
import com.samuel.desafioandroid.service.model.repository.Item;

/**
 * Created by samuel on 18/01/18.
 */

public interface RepositoryAdapterCallback extends MvpView {

    void setRepositoryName(String name);

    void setRepositoryDescription(String description);

    void setRepositoryForksCount(String forksCount);

    void setRepositoryStarsCount(Double stargazersCount);

    void setUserImage(String avatarUrl);

    void setUserName(String name);

    void setFullName(String name);

    void updateFullNameList(Item item);

    void onLoading(boolean b);

    void openActivityDetalhe(DataDetailPullRequest data);

    void showErrorMessage(Throwable throwable);
}
