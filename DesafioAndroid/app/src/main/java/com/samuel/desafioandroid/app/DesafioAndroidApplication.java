package com.samuel.desafioandroid.app;

import android.app.Application;
import android.content.Context;

import com.samuel.desafioandroid.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by samuel on 16/01/18.
 */

public class DesafioAndroidApplication extends Application {

    private static DesafioAndroidApplication instance;

    public static DesafioAndroidApplication getInstance() {
        if (instance == null) {
            instance = new DesafioAndroidApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        configFonts();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        instance = this;
    }

    private void configFonts() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.roboto_regular))
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
