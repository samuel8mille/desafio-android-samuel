package com.samuel.desafioandroid.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.samuel.desafioandroid.R;
import com.samuel.desafioandroid.presentation.presenter.adapter.PullRequestAdapterPresenter;
import com.samuel.desafioandroid.presentation.view.adapter.PullRequestAdapterCallback;
import com.samuel.desafioandroid.service.model.pull_request.PullRequestModel;
import com.samuel.desafioandroid.utils.GlideUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by samuel on 18/01/18.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestHolder> {

    private List<PullRequestModel> pullRequestsList;
    private Context context;

    public PullRequestAdapter(List<PullRequestModel> pullRequestsList) {
        this.pullRequestsList = pullRequestsList;
    }

    @Override
    public PullRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.pull_request_item, null);
        return new PullRequestHolder(view, context);
    }

    @Override
    public void onBindViewHolder(PullRequestHolder holder, int position) {
        holder.setData(pullRequestsList.get(position));
    }

    @Override
    public int getItemCount() {
        return pullRequestsList.size();
    }

    public void updateList(PullRequestModel item) {
        for (int i = 0; i < pullRequestsList.size(); i++) {
            if (pullRequestsList.get(i).getId() == item.getId()) {
                pullRequestsList.set(i, item);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public class PullRequestHolder extends RecyclerView.ViewHolder implements PullRequestAdapterCallback {

        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_desc)
        TextView txtDesc;
        @BindView(R.id.txt_data_pull_request)
        TextView txtData;
        @BindView(R.id.img_user)
        CircleImageView imgUser;
        @BindView(R.id.txt_name)
        TextView txtNome;
        @BindView(R.id.txt_fullname)
        TextView txtFullName;
        @InjectPresenter
        PullRequestAdapterPresenter presenter;
        private Context context;

        PullRequestHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        public void setData(PullRequestModel pullRequestModel) {
            presenter = new PullRequestAdapterPresenter();
            presenter.attachView(this);
            presenter.setData(pullRequestModel);
        }

        @Override
        public void setTitle(String title) {
            txtTitle.setText(title);
        }

        @Override
        public void setDescricao(String descricao) {
            txtDesc.setText(descricao);
        }

        @Override
        public void setDataPullRequest(String data) {
            txtData.setText(data);
        }

        @Override
        public void setImage(String pathImage) {
            GlideUtils.loadExternalImage(pathImage, context, imgUser, R.drawable.user_placeholder);
        }

        @Override
        public void setNomeUsuario(String nome) {
            txtNome.setText(nome);
        }

        @Override
        public void setCategoria(String categoria) {
            txtFullName.setText(categoria);
        }

        @Override
        public void openWeb(String url) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        }

        @Override
        public void updateFullNameList(PullRequestModel item) {
            updateList(item);
        }

        @Override
        public void setFullName(String nameUser) {
            txtFullName.setText(nameUser);
        }

        @Override
        public void showErrorMessage(Throwable throwable) {

        }

        @OnClick(R.id.container_dad)
        void clickDetalhe() {
            presenter.clickDetalhe();
        }
    }
}
