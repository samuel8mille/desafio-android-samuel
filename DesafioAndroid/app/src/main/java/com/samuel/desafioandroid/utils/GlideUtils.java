package com.samuel.desafioandroid.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by samuel on 17/01/18.
 */

public class GlideUtils {

    public static void loadExternalImage(Object url, Context context, final ImageView into, int placeHolder) {
        if (context == null) {
            return;
        }
        Glide.with(context)
                .load(url)
                .centerCrop()
                .crossFade()
                .into(into);
    }
}
