package com.samuel.desafioandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.samuel.desafioandroid.BaseActivity;
import com.samuel.desafioandroid.R;
import com.samuel.desafioandroid.presentation.presenter.adapter.RepositoryAdapterPresenter;
import com.samuel.desafioandroid.presentation.view.activity.HomeView;
import com.samuel.desafioandroid.presentation.view.adapter.RepositoryAdapterCallback;
import com.samuel.desafioandroid.service.model.pull_request.DataDetailPullRequest;
import com.samuel.desafioandroid.service.model.repository.Item;
import com.samuel.desafioandroid.ui.activity.activity.PullRequestActivity;
import com.samuel.desafioandroid.utils.GlideUtils;
import com.samuel.desafioandroid.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by samuel on 16/01/18.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepoViewHolder> {

    private List<Item> itemsList;

    private HomeView homeView;

    public RepositoryAdapter(List<Item> itemsList) {
        this.itemsList = itemsList;
    }

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_item, parent, false), parent.getContext());
    }

    @Override
    public void onBindViewHolder(RepoViewHolder holder, int position) {
        holder.setData(itemsList.get(position));
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void updateList(Item item) {
        for (int i = 0; i < itemsList.size(); i++) {
            if (itemsList.get(i).getId() == item.getId()) {
                itemsList.set(i, item);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void setHomeView(HomeView homeView) {
        this.homeView = homeView;
    }

    public void addItens(List<Item> itemsList) {
        this.itemsList.addAll(itemsList);
        notifyDataSetChanged();
    }

    public void clear() {
        if (itemsList != null) itemsList.clear();
        notifyDataSetChanged();
    }

    public class RepoViewHolder extends RecyclerView.ViewHolder implements RepositoryAdapterCallback {

        @BindView(R.id.txt_repo_name)
        TextView txtRepoName;
        @BindView(R.id.txt_repo_description)
        TextView txtRepoDescription;
        @BindView(R.id.txt_forks_count)
        TextView txtForksCount;
        @BindView(R.id.txt_stars_count)
        TextView txtStarsCount;
        @BindView(R.id.iv_user_image)
        CircleImageView ivUserImage;
        @BindView(R.id.txt_username)
        TextView txtUsername;
        @BindView(R.id.txt_fullname)
        TextView txtFullname;

        @InjectPresenter
        RepositoryAdapterPresenter presenter;

        private Context context;

        RepoViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            ButterKnife.bind(this, itemView);
        }

        @ProvidePresenter
        RepositoryAdapterPresenter createPresenter() {
            return new RepositoryAdapterPresenter();
        }

        public void setData(Item repositoryModel) {
            presenter = new RepositoryAdapterPresenter();
            presenter.attachView(this);
            presenter.setData(repositoryModel);
        }

        @Override
        public void setRepositoryName(String name) {
            txtRepoName.setText(name);
        }

        @Override
        public void setRepositoryDescription(String description) {
            txtRepoDescription.setText(description);
        }

        @Override
        public void setRepositoryForksCount(String forksCount) {
            txtForksCount.setText(forksCount);
        }

        @Override
        public void setRepositoryStarsCount(Double stargazersCount) {
            txtStarsCount.setText(Utils.getNumFormat(stargazersCount));
        }

        @Override
        public void setUserImage(String avatarUrl) {
            GlideUtils.loadExternalImage(avatarUrl, ivUserImage.getContext(), ivUserImage, R.drawable.user_placeholder);
        }

        @Override
        public void setUserName(String name) {
            txtUsername.setText(name);
        }

        @Override
        public void setFullName(String name) {
            txtFullname.setText(name);
        }

        @Override
        public void updateFullNameList(Item item) {
            updateList(item);
        }

        @Override
        public void onLoading(boolean status) {
            homeView.onLoading(status);
        }

        @Override
        public void openActivityDetalhe(DataDetailPullRequest data) {
            PullRequestActivity.open(context, data);
        }

        @OnClick(R.id.container_dad)
        void clickDetails() {
            presenter.clickDetails();
        }

        @Override
        public void showErrorMessage(Throwable throwable) {
            ((BaseActivity) context).showErrors(throwable, null);
        }
    }
}
