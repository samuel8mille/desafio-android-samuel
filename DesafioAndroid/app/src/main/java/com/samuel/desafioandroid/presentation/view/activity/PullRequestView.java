package com.samuel.desafioandroid.presentation.view.activity;

import com.arellomobile.mvp.MvpView;
import com.samuel.desafioandroid.ui.adapter.PullRequestAdapter;

public interface PullRequestView extends MvpView {

    void setTitleToolbar(String title);

    void setIssuesOpen(String issuesOpen);

    void setStart(String star);

    void setFork(String qtdeFork);

    void setAdapter(PullRequestAdapter adapter);

    void showPlaceholder();

}
