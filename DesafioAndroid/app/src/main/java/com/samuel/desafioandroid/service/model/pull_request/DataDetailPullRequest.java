package com.samuel.desafioandroid.service.model.pull_request;

import com.samuel.desafioandroid.service.model.repository.Item;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by samuel on 18/01/18.
 */
@Parcel(Parcel.Serialization.BEAN)
public class DataDetailPullRequest {

    private Item item;
    private List<PullRequestModel> listPullRequest;

    public DataDetailPullRequest() {
    }

    public DataDetailPullRequest(Item item, List<PullRequestModel> listPullRequest) {
        super();
        this.item = item;
        this.listPullRequest = listPullRequest;
    }

    public Item getRepositoryModel() {
        return item;
    }

    public void setRepositoryModel(Item item) {
        this.item = item;
    }

    public List<PullRequestModel> getListPullRequest() {
        return listPullRequest;
    }

    public void setListPullRequest(List<PullRequestModel> listPullRequest) {
        this.listPullRequest = listPullRequest;
    }
}
