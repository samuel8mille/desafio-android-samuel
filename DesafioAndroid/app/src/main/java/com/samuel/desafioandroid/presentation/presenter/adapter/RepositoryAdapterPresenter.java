package com.samuel.desafioandroid.presentation.presenter.adapter;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.samuel.desafioandroid.app.Constants;
import com.samuel.desafioandroid.presentation.view.adapter.RepositoryAdapterCallback;
import com.samuel.desafioandroid.service.RetrofitBase;
import com.samuel.desafioandroid.service.model.pull_request.DataDetailPullRequest;
import com.samuel.desafioandroid.service.model.pull_request.PullRequestModel;
import com.samuel.desafioandroid.service.model.repository.Item;
import com.samuel.desafioandroid.service.model.repository.Owner;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by samuel on 18/01/18.
 */
@InjectViewState
public class RepositoryAdapterPresenter extends MvpPresenter<RepositoryAdapterCallback> {

    private Item item;

    public RepositoryAdapterPresenter() {
    }

    public void setData(Item item) {
        this.item = item;
        getViewState().setRepositoryName(item.getName());
        getViewState().setRepositoryDescription(item.getDescription());
        getViewState().setRepositoryForksCount(item.getForksCount());
        getViewState().setRepositoryStarsCount(item.getStargazersCount());
        getViewState().setUserImage(item.getOwner().getAvatarUrl());
        getViewState().setUserName(item.getOwner().getLogin());
        getOwner(item.getOwner().getLogin());
    }

    public void getOwner(String owner) {
        if (TextUtils.isEmpty(item.getNameUser())) {
            RetrofitBase.getRetrofitApi()
                    .getOwner(owner,
                            Constants.CLIENT_ID,
                            Constants.CLIENT_SECRET)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::getOwnerSuccess,
                            this::getOwnerError);
        } else {
            getViewState().setFullName(item.getNameUser());
        }
    }

    private void getOwnerSuccess(Owner owner) {
        if (owner.getName() != null) {
            item.setNameUser(owner.getName());
            getViewState().setFullName(owner.getName());
            getViewState().updateFullNameList(item);
        }
    }

    private void getOwnerError(Throwable throwable) {
        getViewState().showErrorMessage(throwable);
    }

    public void clickDetails() {
        getViewState().onLoading(true);
        RetrofitBase.getRetrofitApi().getPullRequests(
                item.getOwner().getLogin(),
                item.getName(),
                Constants.CLIENT_ID,
                Constants.CLIENT_SECRET)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onReceivePullRequestsSuccess,
                        this::onReceivePullRequestsError);
    }

    private void onReceivePullRequestsSuccess(List<PullRequestModel> pullRequestsResponse) {
        getViewState().onLoading(false);
        DataDetailPullRequest data = new DataDetailPullRequest(item, pullRequestsResponse);
        getViewState().openActivityDetalhe(data);
    }

    private void onReceivePullRequestsError(Throwable throwable) {
        getViewState().onLoading(false);
        getViewState().showErrorMessage(throwable);
    }
}
