package com.samuel.desafioandroid.utils;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.samuel.desafioandroid.BaseActivity;
import com.samuel.desafioandroid.R;
import com.samuel.desafioandroid.interfaces.DialogInterface;
import com.samuel.desafioandroid.ui.dialog.ErrorDialog;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by samuel on 16/01/18.
 */

public class Utils {

    public static Object jsonToObject(String obj, Type classModel) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .create();
        return gson.fromJson(obj, classModel);
    }

    public static String getNumFormat(double value) {
        if (value == 0) {
            return "-";
        }
        NumberFormat numberFormat = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
        return numberFormat.format(value);
    }

    public static String getDateRequest(String startDate) {
        if (!TextUtils.isEmpty(startDate)) {
            SimpleDateFormat simpleDateFormatString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");
            try {
                Date date = simpleDateFormatString.parse(startDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                return simpleDateFormat.format(calendar.getTime());
            } catch (ParseException e) {
                return "";
            }
        } else {
            return "";
        }
    }

    public static void showDialogThrowable(BaseActivity context, Throwable t, DialogInterface callback) {
        if (t instanceof HttpException) {
            HttpException httpException = ((HttpException) t);
            if (httpException.code() == 401 || httpException.code() == 422 || httpException.code() == 400) {
                try {
                    JsonObject jsonObject = (JsonObject) jsonToObject(httpException.response().errorBody().string(), JsonObject.class);
                    if (jsonObject.has("errors") && jsonObject.get("errors").isJsonArray()) {
                        ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), jsonObject.get("errors").getAsJsonArray().get(0).getAsString(), callback);
                    } else if (jsonObject.has("errors") && jsonObject.get("errors").isJsonObject()) {
                        ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), jsonObject.get("errors").getAsJsonObject().getAsString(), callback);
                    } else if (jsonObject.has("errors")) {
                        ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), jsonObject.get("errors").toString(), callback);
                    }
                } catch (IOException e) {
                    ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), t.getMessage(), callback);
                }
            } else if (httpException.code() == 503) {
                ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), context.getString(R.string.unavailable_service), callback);
            } else {
                ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), t.getMessage(), callback);
            }
        } else if (t instanceof ConnectException || t instanceof IOException) {
            ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), context.getString(R.string.no_internet), callback);
        } else {
            ErrorDialog.showErrorMessage(context.getSupportFragmentManager(), t.getMessage(), callback);
        }
    }
}
