package com.samuel.desafioandroid.presentation.view.activity;

import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpView;
import com.samuel.desafioandroid.ui.adapter.RepositoryAdapter;

public interface HomeView extends MvpView {

    void init();

    void setListenerList(RecyclerView.OnScrollListener scrollListener);

    void onLoading(boolean b);

    void showErrorDialog(Throwable throwable);

    void setAdapter(RepositoryAdapter repositoryAdapter);

}
